import socket
from time import sleep

# This file is intended to connect to wfview,
# and then send the frequency over to gqrx.
# That should keep the two synced up.

# gqrx can make use of a remotely-located RTL-SDR
# by using the rtl_tcp program.

# the end result is that a "SDR Tap" inserted into a radio
# may be remotely controlled, visualized, and kept in sync with the
# tuning (if needed).

wfview = socket.socket();
wfview.connect(("127.0.0.1", 4533));

#gqrx = socket.socket();
gqrx = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
gqrx.connect(("127.0.0.1", 7356));
gqrx.settimeout(2);

while (1):
    wfview.send("f\n".encode());

    wrtn = wfview.recv(1024).decode();

    #print("wfview frequency: [" + str(wrtn).strip() + "]");

    cmd = "F " + str(wrtn);
    #print("about to send: [" + cmd.strip() + "]");

    gqrx.send(cmd.encode());
    grtn = gqrx.recv(1024).decode();
    #print("gqrx return: [" + str(grtn).strip() + "]");
    sleep(0.010);

    gqrx.send("f\n".encode());
    sleep(0.010);
    grtn = gqrx.recv(32).decode();
    #print("gqrx freq: [" + str(grtn).strip() + "]");

    sleep(1);
