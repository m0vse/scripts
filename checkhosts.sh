#!/bin/bash

# List of hosts separated by spaces:
hosts="10.0.0.1 10.0.0.2 enterprise defiant mars edge honeypot piepie"
# delay between complete loops ("pause")
delay=4
# clear between runs? set to true or false 
clear_each_time=true

# - - - - - don't modify below this line - - - - - #

# reference for echo colors:
# https://misc.flogisoft.com/bash/tip_colors_and_formatting

ALIVE=1
DEAD=0
result=0
# Echo to reset character formatting
echo -e "\e[0m"


function checkhost {
    # -w max wait for total command, not including waiting for ping return
    # -W max wait during a ping
    ping -c1 -w1 -W1 -q $1 > /dev/null 2>%1
    if [ $? -ne 0 ]; then
        return $DEAD
    else
        return $ALIVE
    fi
}

if [ $clear_each_time = true ]; then
    clear
fi

# Start Loop
while :
do

    echo -n "Testing hosts"

    block="_________________________________\n"

    alivehosts=0

    for val in $hosts; do

        checkhost $val
        result=$?
        if [ $result -eq $ALIVE ]; then
            #echo -e "Host \e[32m$val\tis alive. \e[39m \e[0m"
            block="$block\nHost \e[32m$val\tis alive.\e[39m\e[0m"
            let "alivehosts+=1"
            echo -en "\e[32m.\e[39m\e[0m"
        else
            #echo -e "Host \e[31m$val\tis dead. \e[39m \e[0m"
            block="$block\nHost \e[31m$val\tis dead.\e[39m\e[0m"
            echo -en "\e[31m.\e[39m\e[0m"
        fi    
    done
    echo ""

    block="$block\n"
    block="$block\n$alivehosts hosts are alive."
    block="$block\n_________________________________"
    

    if [ $clear_each_time = true ]; then
        clear
    fi
    
    echo -e "$block"

    sleep $delay
    
done
# End Loop



echo -e "\e[0mEnding loop."
