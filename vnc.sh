#!/bin/bash
echo "Starting TurboVNC Server"

# Put your desired geometry here: 
GEOMETRY=1680x1050


/opt/TurboVNC/bin/vncserver -geometry $GEOMETRY


### Standard Resolutions (for reference): 

# X61T: 
#    4:3 aspect ratio resolutions: 640×480, 800×600, 960×720, 1024×768 (X61T natiove), 1280×960, 1400×1050, 1440×1080, 1600×1200, 1856×1392, 1920×1440, and 2048×1536.

# iMac and Mac Book Pro: 
#    16:10 aspect ratio resolutions: 1280×800, 1440×900, 1680×1050, 1920×120 (imac 27 inch native), and 2560×1600 (MBP native).

# TV, projectors: 
#    16:9 aspect ratio resolutions: 1024×576, 1152×648, 1280×720 (HD), 1366×768, 1600×900, 1920×1080 (FHD), 2560×1440 (QHD), 3840×2160 (4K), and 7680 x 4320 (8K).

