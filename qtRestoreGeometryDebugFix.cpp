
void wfmain::loadSettings()
{
    // Call this to debug: 
    //debugRestore(settings.value("windowGeometry").toByteArray(), settings.value("windowState").toByteArray());

    // This call resizes the window and messes up the focus: 
    restoreGeometry(settings.value("windowGeometry").toByteArray());

    restoreState(settings.value("windowState").toByteArray());

    // AND THIS IS THE FIX: 
    setWindowState(Qt::WindowActive); // Works around QT bug and returns window focus.
}



void wfmain::debugRestore(QByteArray geometry, QByteArray state)
{
    qDebug() << "----- DEBUG for window Geometry and window State: -----";

    qDebug() << "----- ----- GEOMETRY: ";

    if (geometry.size() < 4)
    {
        qDebug() << "Geometry size too small: " << geometry.size();
        return;
    }
    QDataStream stream(geometry);
    stream.setVersion(QDataStream::Qt_4_0);
    const quint32 magicNumber = 0x1D9D0CB;
    quint32 storedMagicNumber;
    stream >> storedMagicNumber;
    if (storedMagicNumber != magicNumber)
    {
        qDebug() << "Bad magic number: " << storedMagicNumber;
        return;
    }
    const quint16 currentMajorVersion = 3;
    quint16 majorVersion = 0;
    quint16 minorVersion = 0;
    stream >> majorVersion >> minorVersion;
    if (majorVersion > currentMajorVersion)
    {
        qDebug() << "Bad majorVersion: " << majorVersion;
        return;
    }

    // (Allow all minor versions.)
    QRect restoredFrameGeometry;
    QRect restoredGeometry;
    QRect restoredNormalGeometry;
    qint32 restoredScreenNumber;
    quint8 maximized;
    quint8 fullScreen;
    qint32 restoredScreenWidth = 0;
    stream >> restoredFrameGeometry // Only used for sanity checks in version 0
            >> restoredNormalGeometry
            >> restoredScreenNumber
            >> maximized
            >> fullScreen;
    if (majorVersion > 1)
        stream >> restoredScreenWidth;
    if (majorVersion > 2)
        stream >> restoredGeometry;

    qDebug() << "restoredFrameGeometry: " << restoredFrameGeometry;
    qDebug() << "restoredGeometry: " << restoredGeometry;
    qDebug() << "restoredNormalGeometry: " << restoredNormalGeometry;
    qDebug() << "restpredScreenNumber: " << restoredScreenNumber;
    qDebug() << "maximized: " << maximized;
    qDebug() << "fullScreen: " << fullScreen;
    qDebug() << "restoredScreenWidth (v1): " << restoredScreenWidth;
    qDebug() << "restoredGeometry (v2+): " << restoredGeometry;


    // After checking the size against the screen size, this is the call:
    // And this is also where the window seems to lose focus.

    //    if (majorVersion > 2)
    //        setGeometry(restoredGeometry);
    //    else

    // Here is where focus is lost:
    setGeometry(restoredNormalGeometry); // this one is called on my system
    // And this is one of the things that the above call does:
    //data->crect.setSize(restoredNormalGeometry.size().boundedTo(maximumSize()).expandedTo(minimumSize()));

    // HERE IS THE FIX, CALL THIS NEXT:
    this->setWindowState(Qt::WindowActive); // Get focus!

    qDebug() << "----- END DEBUG for window Geometry and window State -----";

}
