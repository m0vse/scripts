# scripts

Useful scripts for my desktop linux systems.

### Ping your network:
If you have a lot of hosts that you seem to periodically ping, here is a nice script to do this for you with a nice output: [checkhosts.sh](https://gitlab.com/eliggett/scripts/-/blob/master/checkhosts.sh)

There are lots of tools that can do this for you, but this tool, checkhosts.sh, does *not* require:
Apache, php, ajax, node.js, a database, python, a splunk subscription, flash, a docker image, or java. 

Get it? It's just a few kb of bash script and the output looks really nice. Just edit the fourth line in the script to show the hosts that you want checked. The script will check in with each host (just a normal ping), and report the status after a run. This script makes good use of color, so make sure you are running in a color terminal. 

### Minecraft:
If you want to play on your LAN using two or  more computers with the same minecraft account, edit the script to change out your username as minecraft requires unique usernames for each player. Just edit start_minecraft.sh. 
Sometimes, you will have to run the script, close the launcher, and then run it again. Microsoft seems to update the minecraft player files on launch sometimes -- typically the first time it is run in a day, it seems. 

### Natural Scrolling:
Edit the  natural_scroll.sh script so that the name of your mouse is included. It is ok to leave other mouse names in, the errors are harmless. 
~~~
xinput --list
~~~

### Launching Merrill Edge Pro on linux
The script ml.sh is designed to assist linux desktop users in launching the JNLP file for Merrill Edge Pro. To use Merrill Edge Pro on linux, one must first install the official Java JRE from java.com. When in firefox, click on the link to launch Merrill Edge Pro, and tell the browser to just save the file. Next, run ml.sh from a terminal. The shell script will find the downloaded file (always selecting the latest one automatically), and will run the javaws ("Java Web Start") binary from your official java install (you can edit ml.sh to point to this location). You can install Java and OpenJDK together if you wish using this method. 


### Blackbox:
Do the following to use the config files:
~~~
mkdir ~/.blackbox
cp blackboxrc ~/.blackboxrc
cp menu ~/.blackbox/menu
~~~

This blackbox config shows how to change themes, restart blackbox, and *adjust the volume control*!

### 32 bit wine:
If you have a separate 32 bit wine install, you can use this script to run applications in that environment. You will need to edit it to point to the right directory.
~~~
32wine.sh /path/to/program.exe
~~~