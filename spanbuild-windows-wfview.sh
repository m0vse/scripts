#!/usr/bin/env bash
### ### ### ### ### ### ###
###
### TO USE: 

BASENAME=wfview-master-`date +%Y%m%d--%H-%M-%S`
EXENAME=$BASENAME.exe
LOGNAME=$BASENAME.txt

DSTDIR=/home/eliggett/Documents/build

REMOTECMD="powershell -Command \Users\eliggett\Documents\projects\wfview-master\build.bat"
REMOTEHOST=10.0.2.121 # VM of Windows

day=$(date +"%u")

echo "The exe will be named " $EXENAME
echo "The resulting file will be placed in " $DSTDIR
echo "The remote host is: " $REMOTEHOST
echo "The remote command is: " $REMOTECMD

echo "Connecting to remote host and running commands."
time ssh $REMOTEHOST "$REMOTECMD" &> /tmp/build.log

echo "Done with ssh."
cd /home/winbuild
ls -l wfview.exe &>> /tmp/build.log
echo "Copying wfview.exe to DSTDIR with timestamp name."
cp wfview.exe $DSTDIR/$EXENAME
echo "Done."
ls -l $DSTDIR/$EXENAME &>> /tmp/build.log
echo "sha256sum: " &>> /tmp/build.log
sha256sum $DSTDIR/$EXENAME &>> /tmp/build.log

echo "Copying file to remote daily build folder:"

# scp

rm $DSTDIR/$EXENAME

echo "Done running script."
