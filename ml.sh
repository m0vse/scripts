#!/bin/bash

# This script is intended to assist in launching the Merrill Edge Market Pro java applet on Linux desktops. 

# To use it, you must log in to Merrill Edge and then click on the link to launch Merrill Edge Market Pro. 

# A popup will open. Wait one second, and then the JNLP file will attempt to load. When prompted as to what to do with the JNLP file, select "save" and save the file to your ~/Downloads directory (you may adjust the location as needed with the $DLF variable below). 
# After the download (which is very brief -- about 10KiB), hop over to a terminal and run this script, ml.sh. This script will launch the downloaded JNLP file properly. 

# Sometimes, the JNLP prompt does not show up. When this happens, close all firefox windows and then open firefox again, log back in to Merrill, and try to launch Merrill Edge Market Pro again. I have found, when this happens, that even manually typing in the URL from the Merrill pop-up window's javascript will produce a 404 error, which indicates to me that sometimes the URL may be malformed at the source. 

# On my system, I have both the Linux Mint openjdk java from the mint repository loaded as well as the official Java JRE from java.com. It is important that you download the latest Java JRE from java.com and keep it updated yourself. On Linux Mint, you will need to download the "Linux (864) 64-bit tar.gz" Java JRE, and definitely not the RPM versions intended for RedHat-based systems. This script exclusively uses the official Java JRE. The OpenJDK and "Ice Tea" web start do not seem to work with Merrill. This script manually calls javaws from the official java install path, which is a variable you can set in the script ($JAVAWS). 
 
# This script was tested with the following versions: 
# Java JRE 1.8.0_251
# Firefox 77.0.1
# Linux Mint 19.1 "Tessa" x86 64-bit
# Desktop Environment: Cinnamon 4.0.10  

# Path to JAVA Web Start binary: 
JAVAWS=/opt/java/jre1.8.0_251/bin/javaws

#Location where web start file is downloaded: 
DLF=~/Downloads

#1: Select in ~/Downloads the most recent mqxxxxx.jnlp file, 
# typically named mqYYYYMMDD.jnlp but there may be multiple copies if downloaded more than once on a given day.


# this commented code does not work properly and often returns the wrong file: 
#unset -v latest
#
#for afile in "$DLF"/mq*.jnlp; do
#  [[ $afile -nt $latest ]] && latest=$afile
#done
#JNLP=$afile

# therefore, we default to relying on ls sorting and formatting: 

JNLP=`ls -t $DLF/mq????????*.jnlp | head -1`

#2: Run the java web start with the file from #1. 

echo "Running Java Web Start with file $JNLP"

$JAVAWS $JNLP

#dry run: echo "Would have run: [$JAVAWS $JNLP]"

# javaws returns promptly while the java applet continues to run. 

echo "Finished calling Java Web Start."

# TODO: remove all matching files to clean out the directory... they expire once used anyway. 
